# Ansible inventory

Official documentation https://docs.ansible.com/ansible/latest/inventory_guide/intro_inventory.html

- inventory defines the managed nodes
- with groups you can run tasks on multiple hosts at the same time.

Once your inventory is defined, you use [patterns](patterns.md) to select the hosts or groups you want Ansible to run against.

The simplest inventory is a single file with a list of hosts and groups.
The default location for this file is `/etc/ansible/hosts`.
You can specify a different inventory file at the command line using the `-i <path>` option or in configuration using `inventory`.

### Simple inventory

```yaml
all:
  children:
    webservers:
      hosts:
        foo.example.com:
        bar.example.com:
    dbservers:
      hosts:
        one.example.com:
        two.example.com:
        three.example.com:
```

You can put each host in more than one group.

### Ranges of hosts

```yaml
  webservers:
    hosts:
      www[01:50].example.com:
```

### Assigning a variable to one machine: host variables

```yaml
webservers:
  hosts:
    host1:
      http_port: 80
      maxRequestsPerChild: 808
    host2:
      http_port: 303
      maxRequestsPerChild: 909
```

### Assigning a variable to many machines: group variables

```yaml
dbservers:
  hosts:
    host1:
    host2:
  vars:
    ntp_server: ntp.atlanta.example.com
    proxy: proxy.atlanta.example.com
```

