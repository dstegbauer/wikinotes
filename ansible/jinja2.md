# Jinja2 templates

## Links

### Jinja2 Tutorial series
* [Part 1 - Introduction and variable substitution](https://ttl255.com/jinja2-tutorial-part-1-introduction-and-variable-substitution/)
* [Part 2 - Loops and conditionals](https://ttl255.com/jinja2-tutorial-part-2-loops-and-conditionals/)
* [Part 3 - Whitespace control](https://ttl255.com/jinja2-tutorial-part-3-whitespace-control/)
* [Part 4 - Template filters](https://ttl255.com/jinja2-tutorial-part-4-template-filters/)
* [Part 5 - Macros](https://ttl255.com/jinja2-tutorial-part-5-macros/)
* [Part 6 - Include and Import](https://ttl255.com/jinja2-tutorial-part-6-include-and-import/)

## Tips and tricks

### Check If Variable – Empty | Exists | Defined | True

* is defined (exists): `{% if variable is defined %}`
* is empty: `{% if variable|length %}`
* is true: `{% if variable is sameas true %}`
* is defined and not empty: `{% if variable is defined and variable|length %}`
* is defined and true: `{% if variable is defined and variable is sameas true %}`
