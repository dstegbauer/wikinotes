# Ansible tips and tricks

Table of Content
- [Create an empty file](#create-an-empty-file)
- [Read name servers (DNS) from /etc/resolv.conf file](#read-name-servers-dns-from-etcresolvconf-file)

### Create an empty file

use the [copy module](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/copy_module.html),
using `force: false` to create a new empty file only when the file does not yet exist
(if the file exists, its content is preserved).

```yaml
- name: ensure file exists
  copy:
    content: ""
    dest: /etc/nologin
    force: false
    group: sys
    owner: root
    mode: 0555
```

_Notes:_
- _the content could be anything, even variable: `content: "{{ version }}"`_
- _with `force: true` the file is recreated every time_


### Read name servers (DNS) from /etc/resolv.conf file

You could use a regex, with the filter
[regex_findall](https://docs.ansible.com/ansible/latest/user_guide/playbooks_filters.html#regular-expression-filters)
in order to achieve this,
especially considering that you might have more than one DNS server defined there.

This will give you a list.

```yaml
- hosts: localhost
  gather_facts: no

  tasks:
    - name: check resolv.conf exists
      stat:
        path: /etc/resolv.conf
      register: resolv_conf
    - name: check nameservers list in resolv.conf
      debug:
        msg: "{{ contents }}"
      vars:
        contents: "{{ lookup('file', '/etc/resolv.conf') | regex_findall('\\s*nameserver\\s*(.*)') }}"
      when: resolv_conf.stat.exists == True
```

### Check Ansible version from inside a playbook
Ansible provides a global dict called ansible_version, dict contains the following
```
"ansible_version": {
        "full": "2.7.4", 
        "major": 2, 
        "minor": 7, 
        "revision": 4, 
        "string": "2.7.4"
    }
```

example using this dict and a when statement.
```yaml
    - name: Print message if ansible version is greater than 2.7.0
      debug:
        msg: "Ansible version is  {{ ansible_version.full }}"
      when: ansible_version.full >= "2.7.4"
```

##### Notes

Ansible has version comparison built in,
so the most stable check to check the ansible version is
`ansible_version.full is version('2.7.4', '>=') –`

With using the major, minor and revision field,
the type must be integer (instead of string) inside the test statement
(e.g. `when: ansible_version.major >= 2`).
This does not apply, when using the is version() method.

