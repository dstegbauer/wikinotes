# Install ansible

## Debian / Ununtu

```shell
sudo apt-get update
sudo apt-get install software-properties-common
sudo apt-add-repository ppa:ansible/ansible $ sudo apt-get update
sudo apt-get install ansible
```

## RHEL8 / Alma Linux 8 / Rocky Linux 8

```shell
yum install python3
alternatives --set python /usr/bin/python3
yum -y install python3-pip
pip3 install ansible --user
```

## Alpine Linux

```shell
apk add ansible
```

## Rocky Linux 9

```shell
sudo dnf install epel-release
sudo dnf install ansible
```
