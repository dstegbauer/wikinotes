# Running ad-hoc Commands

- [Overview](#overview)
- [Node status](#node-status)
- [Execute command](#execute-command)
- [User management](#user-management)
- [Directories and files](#directories-and-files)
- [Package management](#package-management)
- [Service management](#service-management)

## Overview

To execute a command on a node, use the -a option followed by the command you want to run, in quotes.

This will execute `uname -a` on all the nodes in your inventory:
```shell
ansible all -a "uname -a"
```

It is also possible to run Ansible modules with the option -m. The following command would install the package vim on server1 from your inventory:
```shell
ansible <pattern> -m apt -a "name=vim"
```

Before making changes to your nodes, you can conduct a dry run to predict how the servers would be affected by your command. This can be done by including the --check option:
```shell
ansible <pattern> -m apt -a "name=vim" --check
```

##### Ansible command parameters used below

| Short            | Long                      | Desc                                                                                                     |
|------------------|---------------------------|----------------------------------------------------------------------------------------------------------|
| -m MODULE_NAME   | --module-name MODULE_NAME | module name to execute (default=command)                                                                 |
| -a MODULE_ARGS   | --args MODULE_ARGS        | module arguments in either the `key=value` syntax or a JSON string starting with `{` and ending with `}` |
| -i INVENTORY     | --inventory INVENTORY     | specify inventory host path or comma separated host list                                                 |
| -K               | --ask-become-pass         | ask for privilege escalation password                                                                    |
| -b               | --become                  | run operations with become (does not imply password prompting)                                           |
|                  | --become-user BECOME_USER | run operations as this user (default=root)                                                               |
| -u REMOTE_USER   | --user REMOTE_USER        | connect as this user (default=None)                                                                      |
| -B SECONDS       | --background SECONDS      | run asynchronously, failing after X seconds (default=N/A)                                                |
| -P POLL_INTERVAL | --poll POLL_INTERVAL      | set the poll interval if using -B (default=15)                                                           |

##### Connecting as a Different User
By default, Ansible tries to connect to the nodes as your current system user, using its corresponding SSH keypair.
```shell
ansible all -m ping -u sammy
```

##### Using a Custom SSH Key
```shell
ansible all -m ping --private-key=~/.ssh/custom_id
```

##### Using Password-Based Authentication
If you need to use password-based authentication in order to connect to the nodes,
you need to use the option `--ask-pass` (or `-k`)
```shell
ansible all -m ping --ask-pass
```

##### Providing the sudo Password (become root)
Use options `--become --ask-become-pass` (or `-b -K`)
```shell
ansible all -m ping --become --ask-become-pass
```

## Node status

##### Ping
```shell
ansible testserver -m ping -i ansible_hosts -u admin
```

##### Check uptime
```shell
ansible testserver -m command -a uptime 
ansible testserver -m shell -a uptime 
ansible testserver -a uptime
```

##### Memory usage
```shell
ansible testserver -a "free -m" -i ansible_hosts
```

##### Physical memory allocated
```shell
ansible testserver -m shell -a "cat /proc/meminfo|head -2"
```

##### Check free disk space
```shell
ansible testserver -a "df -h"
```

##### list nfs mounts
```shell
ansible testserver -m shell -a 'df -h -T|grep -i nfs'  -i ansible_hosts
```

##### Display memory, cpu and OS distribution and version
```shell
ansible testserver -m setup -i ansible_hosts -a 'filter=ansible_distribution,ansible_distribution_version,ansible_memfree_mb,ansible_memtotal_mb,ansible_processor_cores*,ansible_architecture' 2>/dev/null
```

##### Check listening ports
```shell
ansible testserver -m listen_ports_facts -i prod-ansible-hosts
```


## Execute command

#####  Execute a command as root user
```shell
ansible testserver -m shell -a "id" -b -k
```

##### Execute a command as a different user
```shell
ansible testserver -m shell -a "id" -b -k --become-user tomcat
```


## User management

##### Create a user group
```shell
ansible testserver -b -m group -a "name=weblogic state=present"
```

##### Create a user
```shell
ansible testserver -m user -a "name=weblogic group=weblogic createhome=yes" -b
```


## Directories and files

##### Create a Directory with 755 permission
```shell
ansible testserver -m file -a "path=/opt/oracle state=directory mode=0755" -b
```

##### Create a file with 755 permission
```shell
ansible testserver -m file -a "path=/tmp/testfile state=touch mode=0755"
```

##### Change ownership of a file
```shell
ansible testserver -m file -a "path=/opt/oracle group=weblogic owner=weblogic" -i ansible_hosts -b
```

##### Copy file – local to remote
```shell
ansible testserver -m copy -a "src=~/Downloads/index.html dest=/var/www/html owner=apache group=apache mode=0644"
```

##### Copy directory – local to remote
```shell
ansible testserver -m copy -a "src=~/Downloads/logos dest=/var/www/html/ owner=apache group=apache mode=0644 " -i ansible_hosts -b
```

##### Download a file from URL
```shell
ansible testserver -m get_url -a "url=https://nodejs.org/dist/v14.17.4/node-v14.17.4-linux-x64.tar.xz dest=/tmp mode=0755" -i prod-ansible-hosts
```


## Package management

##### Install a package using yum
```shell
ansible testserver -s -m yum -a "name=httpd state=installed"
```

##### Remove package using yum
```shell
ansible testserver -s -m yum -a "name=httpd state=absent"
```

##### Install and configure python Django application server
```shell
ansible testserver -b -m yum -a "name=MySQL-python state=present"
ansible testserver -b -m yum -a "name=python-setuptools state=present"
ansible testserver -b -m easy_install -a "name=django"
```


## Service management

##### Start the service
```shell
ansible testserver -s -m service -a "name=httod state=started enabled=yes"
```

##### Stop the service
```shell
ansible testserver -s -m service -a "name=httpd state=stop enabled=yes"
```

##### Check service status
```shell
ansible testserver -m service -a "name=httpd" -i ansible_hosts -u admin
```

##### stop, start, restart service – SystemD
Shell module
```shell
ansible webservers -m shell -a "systemctl restart nginx" -b
```

systemd module
```shell
ansible webservers  -m systemd -a "name=nginx state=reloaded" -i prod-ansible-hosts
ansible webservers -m systemd -a "name=nginx state=restarted" -i prod-ansible-hosts
ansible webservers -m systemd -a "name=nginx state=started" -i prod-ansible-hosts
ansible webservers -m systemd -a "name=nginx state=stopped" -i prod-ansible-hosts
```


##### Managing Cron Job and Scheduling

Run the job every 15 minutes
```shell
ansible testserver -s -m cron -a "name='daily-cron-all-servers' minute=*/15 job='/path/to/minute-script.sh'"
```

Run the job every four hours
```shell
ansible testserver -s -m cron -a "name='daily-cron-all-servers' hour=4 job='/path/to/hour-script.sh'"
```

Enabling a Job to run at system reboot
```shell
ansible testserver -s -m cron -a "name='daily-cron-all-servers' special_time=reboot job='/path/to/startup-script.sh'"
```

Scheduling a Daily job
```shell
ansible testserver -s -m cron -a "name='daily-cron-all-servers' special_time=daily job='/path/to/daily-script.sh'"
```

Scheduling a Weekly job
```shell
ansible testserver -s -m cron -a "name='daily-cron-all-servers' special_time=weekly job='/path/to/daily-script.sh'"
```

##### Running operations in the background asynchronous with Polling

Run the operations background using -B and poll the job in the frequent interval -P
```shell
ansible testserver -s -B 3600 -a "yum -y update" 
ansible testserver -m async_status -a "jid=763350539037"
```

If you set -P as 0 it called as fire and forget,
the job id would not be given, and you cannot track the job using async_status as shown above.

##### Reboot the system

Rebooting the host in the background is the best example for fire and forget or async and poll.
```shell
ansible testserver -i inventory -b -B 1 -P 0 -m shell -a "sleep 5 && reboot"
```

there is another ad hoc command available with reboot module to reboot the remote system
```shell
ansible testserver -m reboot -a reboot_timeout=3600 -u admin -i ansible_hosts -b
```
