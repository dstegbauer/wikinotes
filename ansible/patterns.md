# Ansible Patterns: targeting hosts and groups

Official documentation https://docs.ansible.com/ansible/latest/inventory_guide/intro_patterns.html

### Using patterns

ad hoc command
```
ansible <pattern> -m <module_name> -a "<module options>"
```

playbook
```yaml
- name: <play_name>
  hosts: <pattern>
```

### Patterns

You can use either a comma (,) or a colon (:) to separate a list of hosts.
The comma is preferred when dealing with ranges and IPv6 addresses.

| Description | Pattern | Targets |
| --- | --- | --- |
| All hosts | `all`|
| One host | `host1` |
| Multiple hosts | `host1,host2` |
| One group | `webservers` |
| Multiple groups | `webservers,dbservers` | all hosts in webservers plus all hosts in dbservers |
| Excluding groups | `webservers,!atlanta` | all hosts in webservers except those in atlanta |
| Intersection of groups | `webservers,&staging` | any hosts in webservers that are also in staging |

### Using regexes in patterns

You can specify a pattern as a regular expression by starting the pattern with `~`:
```
~(web|db).*\.example\.com
```
