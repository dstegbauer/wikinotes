#!/bin/sh

proto_host=${http_proxy%:*}
http_proxy_host=${proto_host##*://}
http_proxy_port=${http_proxy##*:}

proto_host=${https_proxy%:*}
https_proxy_host=${proto_host##*://}
https_proxy_port=${https_proxy##*:}

./gradlew useLatestVersions --refresh-dependencies \
  -Dhttp.proxyHost=$http_proxy_host -Dhttp.proxyPort=$http_proxy_port \
  -Dhttps.proxyHost=$https_proxy_host -Dhttps.proxyPort=$https_proxy_port \
  -Dhttp.nonProxyHosts="$no_proxy" -Dhttps.nonProxyHosts="$no_proxy" \
  --max-workers 1
is_update_available=$(jq -r .gradle.current.isUpdateAvailable build/dependencyUpdates/report.json)
if [ "$is_update_available" = 'true' ] ; then
  new_version=$(jq -r .gradle.current.version build/dependencyUpdates/report.json)
  /gradlew wrapper --gradle-distribution-url https://artifactory.dbgcloud.io/artifactory/services.gradle.org/gradle-$new_version-bin.zip --max-workers 1
fi
