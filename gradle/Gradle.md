Contents
* [Docs](#docs)
* Tricks
   * [Externalize versions to variables](#externalize-versions-to-variables)
   * [update Gradle](#update-gradle)
* Troubleshooting

# Docs
* [Gradle User Manual](https://docs.gradle.org/current/userguide/userguide.html)
* [Building Java projects](https://docs.gradle.org/current/userguide/building_java_projects.html)
* [Dependency Management for Java Projects](https://docs.gradle.org/current/userguide/dependency_management_for_java_projects.html)

# Tricks

### manual update Gradle
1. find current Gradle version : `./gradlew --version`
1. find latest release: https://gradle.org/releases
1. stop gradle daemon `./gradlew --stop`
1. update Gradle version: `./gradlew --no-daemon wrapper --gradle-version <gradle-version>`
1. delete remnants of old gradle version `rm -rf .gradle`
1. check and commit new gradle version to repository

### Automatic update of versions and of the gradle

see [try-version-updates.sh](try-version-updates.sh)

### Externalize versions to variables
<pre>
ext.versions = [
	sfl4j: '1.7.25'
]
	
dependencies {
	implementation group: 'org.slf4j', name: 'slf4j-api', version: <b>"${versions.sfl4j}"</b>
	implementation group: 'org.slf4j', name: 'slf4j-simple', version: <b>"${versions.sfl4j}"</b>
}
</pre>

# Troubleshooting

## Gradle downloads to ?/.gradle directory when running tasks

**Symptom:** 
Gradle created a `?/.gradle/` in the directory that gradle was run in. We would expect the cache directory to be created at `~/.gradle`.

**Explanation**
The user running the scripts did not have a home directory.
As gradle use following code to get user home: `System.getProperty("user.home");`
When JVM can not found user name in os, it will use `?` as a return. So gradle will create `?/.gradle` for usage.

**Solution**
specify a gradle user home:
```shell
gradle --gradle-user-home=/foo/bar ...
```
or
```shell
GRADLE_USER_HOME=/foo/bar gradle ...
```
