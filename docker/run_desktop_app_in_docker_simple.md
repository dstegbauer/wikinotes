# Running Desktop Apps in Docker (Simple)

For more detailed article see https://gursimar27.medium.com/run-gui-applications-in-a-docker-container-ca625bad4638

We need to have an X server running:
* Linux, FreeBSD, etc ... there already is one 
* macOS ... download XQuartz https://www.xquartz.org/
* Windows ... try e.g. VcXsrv https://sourceforge.net/projects/vcxsrv/

## X Server connection parameters

* macOS: `-e DISPLAY=docker.for.mac.host.internal:0`
* Windows: `-e DISPLAY=host.docker.internal:0`
* Linux: `--net=host -e DISPLAY=:0`

## Run the container

```shell
docker run --rm -it --net=host -e DISPLAY=${DISPLAY} -v /tmp/.X11-unix:/tmp/.X11-unix -v $HOME/.Xauthority:/root/.Xauthority image-name
```


## Troubleshhoting

##### Error: Can't open display: :0

Access control of X is probably in the way.
Run 
```shell
xhost +
```
(from package `x11-xserver-utils`) to completely disable access control.
**Risky!!** - better is to properly handle X authentication.


## Custom image with GUI app

See [Dockerfile-xeyes-alpine](Dockerfile-xeyes-alpine) and build it
```shell
docker build -f Dockerfile-xeyes-alpine \
  --build-arg http_proxy=$http_proxy --build-arg https_proxy=$https_proxy \
  --tag dstegbauer/xeyes \
  .
```
(image size is cca 10MiB)

## Ready-made images on docker hub 

* Gimp `jamesnetherton/gimp`
* Eclipse IDE `psharkey/eclipse`
* Libre Office `woahbase/alpine-libreoffice:x86_64`
* XMind `mdwheele/xmind`
* Firefox `jess/firefox`

## May be Risky
* An app in container runs as root
* An app has full access to screen, thus can make screenshot
* An app has full access to clipboard and input devices (keyboard, mouse, ...)
