# Too long timeout to resolve bad dns name

Configuring DNS resolver failover timeout

The timeout is configurable in `/etc/resolv.conf` with the timeout parameter.
Try adding the line: `options timeout:<desired timeout in seconds>` to /etc/resolv.conf.

The default is RES_TIMEOUT (currently 5,  see  <resolv.h>).
The  value  for  this  option  is silently capped to 30.

There is also `attempts:n` option,
the number of times the resolver will send a query to its name servers before giving up.
The default is RES_DFLRETRY (currently 2, see <resolv.h>).
The value for this option is silently capped to 5.

# Docker (kubernetes too)

The file `/etc/resolv.conf` in docker container is  generated,
so it's not possible to do set that in Dockerfile.
The file in container is owned by root,
so it's not possible to do that in shell script when starting container.

## Docker compose

use `dns-opt` https://docs.docker.com/compose/compose-file/#dns_opt

```yaml
version: '2'
services:
  my-service:
    image: alpine
    dns_opt:
      - timeout:1
      - ndots:1
      - attempts:1
```

## Kubernetes

use `dnsConfig` https://kubernetes.io/docs/concepts/services-networking/dns-pod-service/#pod-s-dns-config

```yaml
spec:
  containers:
  - name: test
    image: alpine
    dnsConfig:
      options:
      - name: timeout
        value: "1"
```
