# "Design Patterns Uncovered" Series on dzone.com

* [Series Overview](https://dzone.com/articles/design-patterns-overview)
* [Design Patterns Refcard](http://refcardz.dzone.com/refcardz/design-patterns)

## Creational Patterns

   * [Abstract Factory](https://dzone.com/articles/design-patterns-abstract-factory)
   * [Builder](https://dzone.com/articles/design-patterns-builder)
   * [Factory Method](https://dzone.com/articles/design-patterns-factory)
   * [Prototype](https://dzone.com/articles/design-patterns-prototype)
   * [Singleton](https://dzone.com/articles/design-patterns-singleton)

## Structural Patterns

   * [Adapter](https://dzone.com/articles/design-patterns-uncovered-0)
   * [Bridge](https://dzone.com/articles/design-patterns-bridge)
   * [Composite](https://dzone.com/articles/design-patterns-composite)
   * [Decorator](https://dzone.com/articles/design-patterns-decorator)
   * [Facade](https://dzone.com/articles/design-patterns-flyweight)
   * [Flyweight](https://dzone.com/articles/design-patterns-flyweight)
   * [Proxy](https://dzone.com/articles/design-patterns-proxy)

## Behavioral Patterns

   * [Chain of Responsibility](https://dzone.com/articles/design-patterns-uncovered-chain-of-responsibility)
   * [Command](https://dzone.com/articles/design-patterns-command)
   * [Interpreter](https://dzone.com/articles/design-patterns-uncovered-14)
   * [Iterator](https://dzone.com/articles/design-patterns-iterator)
   * [Mediator](https://dzone.com/articles/design-patterns-mediator)
   * [Memento](https://dzone.com/articles/design-patterns-memento)
   * [Observer](https://dzone.com/articles/design-patterns-uncovered)
   * [State](https://dzone.com/articles/design-patterns-state)
   * [Strategy](https://dzone.com/articles/design-patterns-strategy)
   * [Template Method](https://dzone.com/articles/design-patterns-template-method)
   * [Visitor](https://dzone.com/articles/design-patterns-visitor)
