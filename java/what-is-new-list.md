# New Features in Java

# Java 9
_https://www.baeldung.com/new-java-9_

* modules https://openjdk.java.net/projects/jigsaw/quick-start
* java.lang.ProcessHandle new functions
* Try-With-Resources  _if the resource is referenced by a final or effectively final variable, a try-with-resources statement can manage a resource without a new variable being declared_
   ```java
   try (new MyAutoCloseable() { }.finalWrapper.finalCloseable) {
     // do some stuff with finalCloseable
   } catch (Exception ex) { }
   ```
* diamond operator in conjunction with anonymous inner classes
* Interface Private Method
* JShell Command Line Tool
* JCMD Sub-Commands
* Variable Handles in _java.lang.invoke_
* Publish-Subscribe Framework (support the Reactive Streams http://www.reactive-streams.org/)
  * java.util.concurrent.Flow
  * java.util.concurrent.SubmissionPublisher
* Immutable Set, see _java.util.Set.of()_
* Optional to Stream (Streams on Optional elements)
   ```java
   List<String> filteredList = listOfOptionals.stream()
  .flatMap(Optional::stream)
  .collect(Collectors.toList());
   ```

# Java 10
_https://www.baeldung.com/java-10-overview_

* `var` keyword
  * https://www.baeldung.com/java-10-local-variable-type-inference
  * [Style Guidelines for Local Variable Type Inference in Java](https://openjdk.java.net/projects/amber/guides/lvti-style-guide)
* Unmodifiable Collections
  * `copyOf`
  * `toUnmodifiable*()`
* Optional*.orElseThrow()

# Java 11
_https://www.baeldung.com/java-11-new-features_

* New New String Methods _isBlank, lines, strip, stripLeading, stripTrailing, repeat_ .
* New File Methods _Files.readString , Files.writeString_
* Collection to an Array _java.util.Collection.toArray(IntFunction)_
* Predicate.not() https://www.baeldung.com/java-negate-predicate-method-reference
* Local-Variable Syntax for Lambda https://www.baeldung.com/java-var-lambda-params
* new HTTP Client https://www.baeldung.com/java-9-http-client
* Nest Based Access Control https://www.baeldung.com/java-nest-based-access-control
* Epsilon - A No-Op Garbage Collector `-XX:+UnlockExperimentalVMOptions -XX:+UseEpsilonGC`
* Java Flight Recorder is free https://www.baeldung.com/java-flight-recorder-monitoring
* JDK Mission Control (JMC) is separate download https://www.baeldung.com/java-flight-recorder-monitoring
* Z garbage collector https://www.baeldung.com/jvm-zgc-garbage-collector

# Java 12
_https://www.baeldung.com/java-12-new-features_

* New String methods: _indent, transform_.
* New _teeing_ collector to the Collectors class
* Compact Number Formatting _NumberFormat getCompactNumberInstance()_
* Switch Expressions https://www.baeldung.com/java-switch
* Pattern Matching for `instanceof` https://www.baeldung.com/java-pattern-matching-instanceof 

# Java 13
_https://www.baeldung.com/java-13-new-features_

* Switch Expressions `yield` statement
  * It's now easy to implement the strategy pattern https://www.baeldung.com/java-strategy-pattern
* Text Blocks https://www.baeldung.com/java-text-blocks
  * triple quotes `"""`
  * new methods _stripIndent, translateEscapes, formatted_
* ZGC now returns uncommitted memory to the operating system by default

# Java 14
_https://www.baeldung.com/java-14-new-features_

* Text Blocks new escape sequences
* Records, the `record` keyword

# Java 15
_https://www.baeldung.com/java-15-new_

* Sealed Classes https://openjdk.java.net/jeps/360
* both ZGC and Shenandoah GC are no longer experimental. (Shenandoah isn't available from all vendor JDKs)

# Java 16
_https://www.baeldung.com/java-16-new-features_

* java.lang.reflect.InvocationHandler invoke default methods of an interface via a dynamic proxy using reflection
* DateTimeFormatter new period-of-day symbol “B“
* Add Stream.toList Method
* Vector API

# Java 17
_https://www.baeldung.com/java-17-new-features_

No language or library changes.

# Java 18

* UTF-8 by Default
* Simple web server

# Java 19

* Virtual Threads (project Loom) https://openjdk.java.net/jeps/425
* Structured concurrency (project Loom) https://openjdk.java.net/jeps/428
* Record Patterns https://openjdk.java.net/jeps/405

