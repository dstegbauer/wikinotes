# The Order of Tests in JUnit

By default, JUnit runs tests using a deterministic but unpredictable order (MethodSorters.DEFAULT).

#### Links
* https://www.baeldung.com/junit-5-test-order

## Alphanumeric Order

* [MethodOrderer.MethodName](https://junit.org/junit5/docs/current/api/org.junit.jupiter.api/org/junit/jupiter/api/MethodOrderer.MethodName.html) to sort test methods based on their names and their formal parameter lists
* [MethodOrderer.DisplayName](https://junit.org/junit5/docs/current/api/org.junit.jupiter.api/org/junit/jupiter/api/MethodOrderer.DisplayName.html) to sort methods alphanumerically based on their display names.
* 
```java
@TestMethodOrder(MethodOrderer.MethodName.class)
public class AlphanumericOrderUnitTest {

    @Test
    void myATest() {
        // run first
    }

    @Test
    void myBTest() {
        // run second
    }
}
```


## @Order Annotation

[MethodOrderer.OrderAnnotation](https://junit.org/junit5/docs/current/api/org.junit.jupiter.api/org/junit/jupiter/api/MethodOrderer.OrderAnnotation.html)
enforces tests to run based on `@Order` annotation.

```java
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class OrderAnnotationUnitTest {
    @Test
    @Order(1)
    void firstTest() {
        output.append("a");
    }

    @Test
    @Order(2)
    void secondTest() {
        output.append("b");
    }
}
```

## Random Order

[MethodOrderer.Random](https://junit.org/junit5/docs/current/api/org.junit.jupiter.api/org/junit/jupiter/api/MethodOrderer.Random.html)
orders test methods pseudo-randomly.

```java
@TestMethodOrder(MethodOrderer.Random.class)
public class RandomOrderUnitTest {
    @Test
    void myATest() {
        output.append("A");
    }

    @Test
    void myBTest() {
        output.append("B");
    }
}
```
