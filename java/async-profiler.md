# async-profiler

async-profiler https://github.com/async-profiler/async-profiler
is a sampling profiler for any JDK based on the HotSpot JVM.
It has low overhead and doesn’t rely
on [JVMTI](https://en.wikipedia.org/wiki/Java_Virtual_Machine_Tools_Interface).

## Installation

* download the latest [release of async-profiler](https://github.com/jvm-profiling-tools/async-profiler/releases)
* transfer and unpack the tarball on the target machine

## Usage

### Get list of available metrics

```shell
asprof list <PID>
```

### Start profiling

```shell
asprof -d 300 -e cpu,alloc,lock,context-switches -o jfr -f profiled-app.jfr <PID>
```

* `-d 300` - duration of profiling in seconds
* `-e cpu,alloc,lock,context-switches` - list of events to profile, see also the output of `list` command
* `-o jfr` - output format
* `-f profiled-app.jfr` - output file
* `<PID>` - process ID of the target application

#### JFR output format

this format can be opened in
* Java Mission Control (JMC)
  *  Oracle http://jdk.java.net/jmc
  * AdoptOpenJDK - https://adoptopenjdk.net/jmc.html ( or https://ci.adoptopenjdk.net/view/JMC/job/jmc-latest/)
  * Zulu Mission Control - https://www.azul.com/products/zulu-mission-control/
  * Liberica Mission Control - https://bell-sw.com/pages/lmc/
* VisualVM https://visualvm.github.io/download.html
* JProfiler https://www.ej-technologies.com/jprofiler/download (commercial)

## Other links

* (Flight Recorder)[https://docs.oracle.com/en/java/java-components/jdk-mission-control/9/user-guide/using-jdk-flight-recorder.html]
