# Java heap dump

## Create heap dump

#### jmap

```shell
jmap -dump:live,file=<file-path> <pid> 
```

Note: It’s quite important to pass “live” option.
If this option is passed, then only live objects in the memory are written into the heap dump file.
If this option is not passed, all the objects, even the ones which are ready to be garbage collected are printed in the heap dump file.

#### -XX:+HeapDumpOnOutOfMemoryError

When an application experiences java.lang.OutOfMemoryError

```
-XX:+HeapDumpOnOutOfMemoryError -XX:HeapDumpPath=<file-path>
```

#### jcmd

```shell
jcmd <pid> GC.heap_dump <file-path>
```

#### VisualVM

VisualVM is currently only distributed as a standalone tool
https://visualvm.github.io/download.html

#### JMX

There is a `com.sun.management:type=HotSpotDiagnostic` MBean.
This MBean has ‘dumpHeap’ operation.
Invoking this operation will capture the heap dump.
‘dumpHeap’ operation takes two input parameters:

* outputFile: File path where heap dump should be written
* live: When ‘true’ is passed only live objects in heap are captured
* 
You can use JMX clients such as JConsole, jmxsh, or Java Mission Control to invoke this MBean operation.

#### Programmatic Approach

article from Oracle which gives the source code for capturing heap dumps
from the application by invoking the MBean JMX Bean
[com.sun.management:type=HotSpotDiagnostic](https://docs.oracle.com/javase/8/docs/jre/api/management/extension/com/sun/management/HotSpotDiagnosticMXBean.html#dumpHeap-java.lang.String-boolean-)


## Analyze heap dump

#### VisualVM

VisualVM can take and browse Heap Dumps
https://visualvm.github.io/download.html

#### Eclipse MAT

https://www.eclipse.org/mat/

In Eclipse MAT, two types of object sizes are reported:

* **Shallow heap size**: the amount of memory consumed by one object.
* **Retained heap size**: the amount of memory that will be freed when an object is garbage collected.

Views
* Histogram View shows into what objects currently exist
* Dominator Tree is used to identify the retained heap.
* Leak Suspects Report is used to find a suspected big object or set of objects


#### Heap Hero

http://heaphero.io/

Online only, 1 user, 5 per month, 50MiB file size limit

#### YourKit

http://www.yourkit.com/

Payed

#### JXRay

https://jxray.com/download

Payed, trial for 7 tool runs 