# Make bootable usb stick from iso file

_Examples use Linux Mint 21 Mate edition._

## Verify integrity of downloaded iso file

Note: the sha256sum.txt contains checksums for all linux mint 21 "flavors" (cinnamon, mate, xfce)
```shell
cd ~/Downloads/linux-iso
curl -o linuxmint-21-sha256sum.txt 'https://ftp.heanet.ie/mirrors/linuxmint.com/stable/21/sha256sum.txt'
sha256sum -c linuxmint-21-sha256sum.txt
```

##  Check authenticity of checksum file

```shell
cd ~/Downloads/linux-iso
curl -o linuxmint-21-sha256sum.txt.gpg https://ftp.heanet.ie/mirrors/linuxmint.com/stable/21/sha256sum.txt.gpg
```

Import the Linux Mint signing key:
```shell
gpg --keyserver hkp://keyserver.ubuntu.com:80 --recv-key "27DE B156 44C6 B3CF 3BD7  D291 300F 846B A25B AE09"
```

If gpg complains about the key ID, try the following commands instead:
```shell
gpg --keyserver hkp://keyserver.ubuntu.com:80 --recv-key A25BAE09
gpg --list-key --with-fingerprint A25BAE09
```

Check the output of the last command, to make sure the fingerprint is
`27DE B156 44C6 B3CF 3BD7 D291 300F 846B A25B AE09` (with or without spaces).

Verify the authenticity of sha256sum.txt:
```shell
gpg --verify linuxmint-21-sha256sum.txt.gpg linuxmint-21-sha256sum.txt
```
The output of the last command should tell you that the file signature is good
and that it was signed with the `A25BAE09` key.
GPG might warn you that the Linux Mint signature is not trusted by your computer.
This is expected and perfectly normal.

## Bake bootable usb

#### GUI (on Linux Mint)

Right-click the ISO file and select Make Bootable USB Stick,
or launch Menu ‣ Accessories ‣ USB Image Writer.

#### Command Line

```shell
dd bs=4M if=linuxmint-21-mate-64bit.iso of=/dev/sda
```

## Verify image is written OK

See https://unix.stackexchange.com/questions/75483/how-to-check-if-the-iso-was-written-to-my-usb-stick-without-errors

1. Flush buffers of usb device
   ```shell
   blockdev --flushbufs /dev/sda
   ```
2. Remove usb stick physically from connector
3. Reinsert it
4. Check integrity (as root)
   ```shell
   umount /dev/sda1
   cmp -n $(stat -c '%s' linuxmint-21-mate-64bit.iso) linuxmint-21-mate-64bit.iso /dev/sda
   ```
