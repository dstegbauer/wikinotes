
# run command and save the output (stderr, stdout) to a file
# optionally print the log lines on screen on-the-go
#
# It is a bit tricky as it does not use shell pipes. Posix shell (like dash or ash/busybox) has no access
# to exit codes of particular pipe commands, just the last one.
# To overcome it, we run the command on background, redirecting output to a file.
# In parallel we (optionally, if not quiet) run "tail -f" in background too.
# Finally we wait for completion of the command and terminate the "tail" too.
#
# Parameters
# 1 ... log file name
# the rest ... command to run
run_logged() {
  local logfile=$1
  shift
  "$@" >> $logfile 2>&1 &
  local cmd_pid=$!
  tail -f $logfile &
  local tail_pid=$!
  # do not fail on error
  set +e
  wait $cmd_pid
  kill $tail_pid
  set -e
  return $result_code
}
