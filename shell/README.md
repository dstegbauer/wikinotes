# Unix Shell (posix & bash), sed, awk, find, grep ... 

## Mostly shell
* [BusyBox Ash vs Dash vs Bash](ash-dash-bash-comparison.md) 
* [Redirections](redirect.md)
* [The "proper" way to use find and read together to process files](find-and-read-together-to-process-files.md)
* 

<!--- awk oneliners --->
<!--- sed oneliners --->

## Snippets

* [Run command logging its output to stdout and to a file, and catch return code](run-logged.sh)
