# The "proper" way to use find and read together to process files

Posix ❌ Busybox ✅ Dash ❌ Bash ✅

stolen from: https://www.computerhope.com/unix/bash/read.htm
```shell
while IFS= read -r -d $'\0' file; do
   echo "$file"
done < <(find . -print0)
```

It's especially useful when you want to do something to a lot of files that have odd or unusual names.

Note: the `read -d` and `$'\0'` and `<(` constructs are _bashisms_, but they fortunately work in Alpine Linux /bin/sh shell

```
while IFS=
```
`IFS=` (with nothing after the equals sign) sets the internal field separator to "no value".
Spaces, tabs, and newlines are therefore considered part of a word, which preserves white space in the file names.

Note that `IFS=` comes after `while`, ensuring that IFS is altered only inside the while loop.

```
read -r
```
Using the `-r` option is necessary to preserve any backslashes in the file names.

```
-d $'\0'
```
The `-d` option sets the newline delimiter.
Here, we're setting it to the NULL character, ASCII code zero.
(An escaped zero enclosed in single quotes, preceded by a dollar sign, is interpreted by bash as NULL.
For more information, see: _Expanding strings with interpreted escapes_ in the bash documentation.)

We're using NULL as the line delimiter because Linux file names can contain newlines, so we need to preserve them.
However, a NULL can never be part of a Linux file name, so that's a reliable delimiter to use.

```
< <(find . -print0)
```
Here, `find . -print0` creates a list of every file in and under . (the working directory) and delimit all file names with a NULL.
When using -print0, all other arguments and options must precede it, so make sure it's the last part of the command.

Enclosing the find command in `<( ... )` performs **process substitution**:
the output of the command can be read like a file. In turn, this output is redirected to the while loop using the first `"`<`.

Every iteration of the `while` loop, `read` reads one word (a single file name) and puts that value into the variable `file`,
which we specified as the last argument of the read command.

When there are no more file names to read, `read` returns false, which triggers the end of the `while` loop, and the command sequence terminates.
