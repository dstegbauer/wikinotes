# Redirections

| Redirection           | Description                                                                                                                         | Busybox | Dash | Bash |
|-----------------------|-------------------------------------------------------------------------------------------------------------------------------------|:-------:|:----:|:----:|
| `cmd < file.txt`      | send content of file to command stdin                                                                                               |    ✅    |  ✅   |  ✅   |
| `cmd > file.txt`      | redirect stdout to a truncated file                                                                                                 |    ✅    |  ✅   |  ✅   |
| `cmd >> file.txt`     | redirect stdout appending to a file                                                                                                 |    ✅    |  ✅   |  ✅   |
| `cmd >file.txt 2>&1`  | redirect both stdout and stderr to a truncated file                                                                                 |    ✅    |  ✅   |  ✅   |
| `cmd >>file.txt 2>&1` | redirect both stdout and stderr appending to a file                                                                                 |    ✅    |  ✅   |  ✅   |
| `<&-`                 |
| `>&-`                 |
| `&> file.txt`         | the same as `>file.txt 2>&1` , preferred form                                                                                       |   ❌     |  ❌   |  ✅   |
| `>& file.txt`         | the same as `>file.txt 2>&1`                                                                                                        |   ❌     |  ❌   |  ✅   |
| `&>> file.txt`        | the same as `>>file.txt 2>&1`                                                                                                       |   ❌     |  ❌   |  ✅   |
| `<<delim`             | here document. All lines of the here-document are subjected to parameter expansion, command substitution, and arithmetic expansion. |    ✅    |  ✅   |  ✅   |
| `<<"delim"`           | here document. The character sequence `\<newline>` is ignored, and `\` must be used to quote the characters `\`, `$`, and backtick. |    ✅    |  ✅   |  ✅   |
| `<<-delim`            | here document. All leading **tab characters** are stripped from input lines and the line containing delimiter.                      |    ✅    |  ✅   |  ✅   |
| `<<<word`             | here string. The word is expanded and supplied to the command on its standard input.                                                |   ❌     |  ❌   |  ✅   |


## Order of redirections is significant

Shell executes the redirects from left to right as follows:

1. `>file.txt`: Open file.txt and redirect stdout there.
2. `2>&1`: Redirect stderr to "where stdout is currently going". In other words, the `&1` reuses the file descriptor which stdout currently uses.

| Example               | Effect                                                                                                                                                                         |
|-----------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `cmd > file.txt 2>&1` | directs both standard output and standard error to the file dirlist.txt                                                                                                        |
| `cmd 2>&1 > file.txt` | directs only the standard output to file dirlist.txt, because the standard error was duplicated from the standard output before the standard output was redirected to dirlist. |

## Redirect an entire loop

Posix ✅ Busybox ✅ Dash ✅ Bash ✅

```shell
for i in 1 2 3; do
  echo "Now processing $i"
  # other commands
done > file 2>&1
```

## All output of a script should go into a file

Posix ✅ Busybox ✅ Dash ✅ Bash ✅

Redirect both standard output and standard error to "log.txt" (from everyting which runs in the script)
```shell
exec > log.txt 2>&1
```

## Command group output redirection

Posix ✅ Busybox ✅ Dash ✅ Bash ✅

```shell
{
  date
  # other commands
  echo done
} > messages.log 2>&1
```

## Bash special filenames when they are used in redirections

Posix ❌ Busybox ❌ Dash ❌ Bash ✅

| File name          | Description                                       |
|--------------------|---------------------------------------------------|
| /dev/fd/number     | file descriptor number is duplicated              |
| /dev/stdin         | file descriptor 0 is duplicated                   |
| /dev/stdout        | file descriptor 1 is duplicated                   |
| /dev/stderr        | File descriptor 2 is duplicated                   |
| /dev/tcp/host/port | open a TCP connection to the corresponding socket |
| /dev/udp/host/port | open a UDP connection to the corresponding socket |

