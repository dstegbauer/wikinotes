# Shell parameter expansion

* https://www.gnu.org/savannah-checkouts/gnu/bash/manual/bash.html#Shell-Parameter-Expansion
* https://linux.die.net/man/1/ash

Note: dash and ash have the same parameter expansions.

Note: Bash arrays are not covered here

| Expansion          | bash | ash | Desc                            |
|--------------------|------|-----|---------------------------------|
| `${param:-word}`   | Y    | Y   | use default value               |
| `${param:=word}`   | Y    | Y   | assign default value            |
| `${param:?[word]}` | Y    | Y   | indicate error if Null or Unset |
| `${param:+word}`   | Y    | Y   | use alternative value           |

String processing.
Pattern matching notation (see [Shell Patterns](#shell-patterns)) is used to evaluate the patterns.

| Expansion                  | bash | ash | Desc                                                                              |
|----------------------------|------|-----|-----------------------------------------------------------------------------------|
| `${#param}`                | Y    | Y   | string length                                                                     |
| `${param%pattern}`         | Y    | Y   | remove smallest suffix pattern                                                    |
| `${param%%pattern}`        | Y    | Y   | remove largest suffix pattern                                                     |
| `${param#pattern}`         | Y    | Y   | remove smallest prefix pattern                                                    |
| `${param##pattern}`        | Y    | Y   | remove largest prefix pattern                                                     |
| `${param:offset}`          | Y    | N   | substring (negative offset possible)                                              |
| `${param:offset:length}`   | Y    | N   | substring (negative offset and length possible}                                   |
| `${param/pattern/string}`  | Y    | N   | only the first match is replaced                                                  |
| `${param//pattern/string}` | Y    | N   | all matches of pattern are replaced                                               |
| `${param/#pattern/string}` | Y    | N   | must match at the beginning                                                       |
| `${param/%pattern/string}` | Y    | N   | must match at the end                                                             |
| `${param^pattern}`         | Y    | N   | converts only the first character lowercase letters matching pattern to uppercase |
| `${param^^pattern}`        | Y    | N   | converts each matched character lowercase letters matching pattern to uppercase   |
| `${param,pattern}`         | Y    | N   | converts only the first character matching uppercase letters to lowercase         |
| `${param,,pattern}`        | Y    | N   | converts each matched character matching uppercase letters to lowercase           |
| `${param@operator}`        | Y    | N   | transformation of the value of parameter or information about parameter itself    |

Indirect expansion

| Expansion     | bash | ash | Desc                                                               |
|---------------|------|-----|--------------------------------------------------------------------|
| `${!name}`    | Y    | N   | Use value of _name_ as name of other parameter                     | 
| `${!prefix*}` | Y    | N   | names of variables whose names begin with prefix                   |
| `${!prefix@}` | Y    | N   | within double quotes each variable name expands to a separate word |

## Shell Patterns

* `*` matches any string of characters
* `?` matches any single character
* `[]` a character class matches any of the characters between the square brackets
* `!` complement character class if it is first character after [