# Feature comparison of various Linux shells

✅❌

| Construct  | Description                | Busybox Ash | Dash | Bash |
|------------|----------------------------|-------------|------|------|
| `$'\0'`    | Character with ascii value | Y           | -    | Y    |
| `read -d`  | sets the newline delimiter | Y           | -    | Y    |
| `<( ... )` | process substitution       | Y           | -    | Y    |

