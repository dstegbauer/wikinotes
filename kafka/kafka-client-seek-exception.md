# Kafka client IllegalStateException: No current assignment for partition XYZ

https://stackoverflow.com/questions/54480715/no-current-assignment-for-partition-occurs-even-after-poll-in-kafka

## Symptom

The newly created consumer throws exception after call to seekToEnd.

Kotlin example (shortened):
```kotlin
val consumer = KafkaConsumer(consumerProps, StringDeserializer(), StringDeserializer())
consumer.subscribe(listOf(topic))
val partitions = consumer.partitionsFor(topic)
consumer.seekToEnd(
            listOf(
                TopicPartition(partitions[0].topic(), partitions[0].partition()),
            )
        )
```

Exception:
```
Caused by: java.lang.IllegalStateException: No current assignment for partition mytopic-0
	at org.apache.kafka.clients.consumer.internals.SubscriptionState.assignedState(SubscriptionState.java:370)
	at org.apache.kafka.clients.consumer.internals.SubscriptionState.lambda$requestOffsetReset$3(SubscriptionState.java:643)
	at java.base/java.util.Collections$SingletonList.forEach(Collections.java:4966)
	at org.apache.kafka.clients.consumer.internals.SubscriptionState.requestOffsetReset(SubscriptionState.java:641)
	at org.apache.kafka.clients.consumer.KafkaConsumer.seekToEnd(KafkaConsumer.java:1687)
```

Note: using org.apache.kafka:kafka-clients:3.3.1

## The cure

Answer from Stack Overflow:
The correct way to reliably seek and check current assignment is to wait for
the onPartitionsAssigned() callback after subscribing. On a newly created
(still not connected) consumer, calling poll() or seek() once does not guarantee
it will immediately be connected and assigned partitions.

Kotlin example:
```kotlin
val consumer = KafkaConsumer(consumerProps, StringDeserializer(), StringDeserializer()))
consumer.subscribe(
    listOf(topic),
    object : ConsumerRebalanceListener {
        override fun onPartitionsRevoked(partitions: Collection<TopicPartition>) {}
        override fun onPartitionsAssigned(partitions: Collection<TopicPartition>) {
            LOG.trace("Assigned {}", partitions)
            consumer.seekToEnd(partitions)
        }
    }
)
```
