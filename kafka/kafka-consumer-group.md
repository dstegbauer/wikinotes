# Kafka Consumer Group and Client Id

ToC
* [Configuration](#configuration)
* [Group Id](#group-Id)
* [Client Id](#client-id)
* [Auto Offset Reset](#auto-offset-reset)
* [Links](#links)

## Configuration

* mandatory property _group.id_
  * java `props[ConsumerConfig.GROUP_ID_CONFIG] = "my_group`
* optional property _consumer.id_
  * java `props[CommonClientConfigs.CLIENT_ID_CONFIG] = "my_consumer"`
* property _auto.offset.reset_
  * java `props[ConsumerConfig.AUTO_OFFSET_RESET_CONFIG] = "value"`
  * value `earliest`
  * value `latest` (kafka default)

## Group Id
* All the Consumers in a group have the same group.id.
* Only one Consumer reads each partition in the topic.
* The maximum number of Consumers is equal to the number of partitions in the topic.
  If there are more consumers than partitions, then some consumers will remain idle.
* A Consumer can read from more than one partition.


## Client Id

In addition to `group.id`, each consumer also identifies itself to the Kafka broker using `consumer.id`.
This is used by Kafka to identify the currently _ACTIVE_ consumers of a particular consumer group.
This setting has no functional effect, it's only used to tag requests, so they can be matched in the broker's log if needed.

![A consumer group reading from a topic](img/consumer-group-reading-from-a-topic.png)

![Two consumer groups reading from a topic](img/two-groups-reading-from-a-topic.png)

## Auto Offset Reset

The auto offset reset consumer configuration defines how a consumer should
behave when consuming from a topic partition when there is no initial offset.
This is most typically of interest when a new consumer group has been defined
and is listening to a topic for the first time.
This configuration will tell the consumers in the group whether to read
from the beginning or end of the partition.

That property only applies if the broker has no committed offset for the group/topic/partition.

i.e. the first time the app runs or if the offsets expire
(with modern brokers, the default expiration is when the consumer has not run for 7 days - 7 days after the last consumer left the group).

When there is already a committed offset, that is used for the start position,
and this property is ignored.

For most use cases, `earliest` is used, but `latest` is the default,
which means "new" consumers will start at the end and won't get any records already in the topic.

So the "risk" is, if you don't run your app for a week, you'll get any unexpired records again.
You can increase `offset.retention.minutes` to avoid that.

## Links

* Explain Consumer Group In Kafka https://sagarkudu.medium.com/explain-consumer-group-in-kafka-1c61fa3a77b
* Introduction to Kafka Consumer Group https://www.educba.com/kafka-consumer-group/
* 