## ToC

* [Command line tools](#command-line-tools)
   * [Topics](#topics)
   * [Consumer groups](#consumer-groups)
* [Fixing errors](#fixing-errors)
* [Links](#links)

See also
* https://kafka-tutorials.confluent.io/
* https://www.confluent.io/blog/category/apache-kafka/


# Command line tools

## Topics

##### List topics
```
kafka-topics --list --bootstrap-server localhost:9092
```

##### Describe topic
```
kafka-topics --describe --bootstrap-server localhost:9092 --topic topic_name
```

##### Create topic
```
kafka-topics --create --bootstrap-server localhost:9092 --topic topic_name \
 --partitions 1 --replication-factor 1
```

##### Create compacted topic
```
kafka-topics --create --bootstrap-server localhost:9092 --topic topic_name \
 --replication-factor 1 --partitions 1 \
 --config "cleanup.policy=compact" \
 --config "delete.retention.ms=100"  --config "segment.ms=100" --config "min.cleanable.dirty.ratio=0.01"
```

##### Configure topic
```
kafka-topics --alter --bootstrap-server localhost:9092 --topic topic_name \
 --config cleanup.policy=delete \
 --config delete.retention.ms=3600000
```

##### Delete topic
```
kafka-topics --delete --bootstrap-server localhost:9092 --topic topic_name
```

##### Delete topic content

a bit unpredictable, since kafka decides when the cleanup will be triggered, 
for large partitions it's done within cca < 5minutes (as per my experience):
Simply set retention to 1 ms, wait for the data to be deleted (checking is awkward),
then set back to original value
```
kafka-configs --describe --bootstrap-server localhost:9092 --entity-type topics --entity-name topic_name
# Configs for topic 'topic_m7tshrdate4_m7-core-events' are cleanup.policy=delete,min.insync.replicas=2,retention.ms=604800000,segment.ms=604800000
kafka-configs --alter --bootstrap-server localhost:9092 --entity-type topics --entity-name topic_m7tshrdate4_m7-core-events --add-config retention.ms=1
kafka-configs --alter --bootstrap-server localhost:9092 --entity-type topics --entity-name topic_m7tshrdate4_m7-core-events --add-config retention.ms=604800000
```

delete the data (requires a json config file), but does the job immediately
the json file (example for a one partition core event topic):
{ "partitions": [ { "topic": "topic_m7tshrdate4_m7-core-events", "partition": 0, "offset": -1 } ], "version": 1 }
then run following command (as root on broker):
kafka-delete-records --bootstrap-server m7tshrdintekbr1:9092,m7tshrdintekbr2:9092,m7tshrdintekbr3:9092 --command-config /shrd/kafka/config/admin.properties --offset-json-file delete.json
Note that you must provide all partitions of that topic, if we want to wipe them all, luckily core events has only one partition - partition zero.


## Consumer groups

##### List groups
```
kafka-consumer-groups --bootstrap-server localhost:9092 --all-groups --describe
```

##### List group details:

```
kafka-consumer-groups --bootstrap-server localhost:9092 --describe --group group-name
```

## Records (topic content)

#### Produce records
```
kafka-console-producer --broker-list localhost:9092 \
 --property parse.key=true --property key.separator=: \
 --topic topic_name
``` 

#### Print content of topic

kafka running inside docker container
```
kafka-console-consumer --bootstrap-server localhost:9092 \
  --skip-message-on-error \
  --property print.key=true --property key.separator=" <<<<>>>> " \
  --from-beginning --topic topic_name
```
   * `--max-messages 10` to print max 10 messages. If not set or if there is fewer messages, consumption is continual.
   * `--timeout-ms 1000` exit if no message is available for consumption for the 1 sec

#### Count messages in a Kafka topic

##### Using ConsumerGroupCommand
The “LAG” column in the output indicates the COUNT of messages in the specific partition within a Topic.
```
kafka-run-class kafka.admin.ConsumerGroupCommand \
  --bootstrap-server localhost:9092 \
  --describe \
  --group <GROUP_NAME>
```

##### Using GetOffsetShell
```
kafka-run-class kafka.tools.GetOffsetShell \
  --broker-list <HOST1:PORT,HOST2:PORT> \
  --topic <TOPIC_NAME>
```

##### Using Kafkacat
Consume the entire Kafka topic using kafkacat, and count how many messages are read.
The _kafkacat_ command is in the docker images _edenhill/kafkacat_ or _solsson/kafkacat:alpine_ (debian based version exists too).
```
kafkacat -b localhost:9092 -C -t pageviews -e -q | wc -l
```

##### Using kafka-console-consumer
```
kafka-console-consumer.sh \
  --from-beginning \
  --bootstrap-server localhost:9092 \
  --property print.key=true \
  --property print.value=false \
  --property print.partition \
  --topic <TOPIC_NAME> | tail -n 10|grep "Msg Count="
```


# Fixing errors

### LEADER_NOT_AVAILABLE
Link: https://www.hadoopinrealworld.com/how-to-fix-the-leader_not_available-error-in-kafka/

#### Symptom
log message _Error while fetching metadata with correlation id 1 : {topic_name=LEADER_NOT_AVAILABLE}_

#### Solution:
When you start Kafka brokers, the brokers can listen to multiple listeners. Listeners are nothing but hostname or IP, port and protocol combination. 

The below properties go in server.properties  file on each Kafka broker. Note that `advertised.listeners` is the important property that will help you solve the issue.

* `listeners`  – comma separated hostnames with ports on which Kafka brokers listen to 
* `advertised.listeners`  – comma separated hostnames with ports which will be passed back to the clients. Make sure to include only the hostnames which will be resolved at the client (producer or consumer) side for eg. public DNS.
* `listener.security.protocol.map`  – includes the supported protocols for each listener
* `inter.broker.listener.name`  – listeners to be used for internal traffic between brokers. These hostnames included here don’t need to be resolved at the client side but need to be resolved at all the brokers in the cluster.

The "server.properties" file:
```
listeners: LISTENER_PUBLIC://kafka0:29092,LISTENER_INTERNAL://localhost:9092
advertised.listeners: LISTENER_PUBLIC://kafka0:29092,LISTENER_INTERNAL://localhost:9092
listener.security.protocol.map: LISTENER_PUBLIC:PLAINTEXT,LISTENER_INTERNAL:PLAINTEXT
inter.broker.listener.name: LISTENER_PUBLIC
```

##### Docker setup
Use the below properties (env variables?) if you are running your Kafka in Docker
```
KAFKA_LISTENERS: LISTENER_PUBLIC://kafka0:29092,LISTENER_INTERNAL://localhost:9092
KAFKA_ADVERTISED_LISTENERS: LISTENER_PUBLIC://kafka0:29092,LISTENER_INTERNAL://localhost:9092
KAFKA_LISTENER_SECURITY_PROTOCOL_MAP: LISTENER_PUBLIC:PLAINTEXT,LISTENER_INTERNAL:PLAINTEXT
KAFKA_INTER_BROKER_LISTENER_NAME: LISTENER_PUBLIC
```

Note that inside https://www.testcontainers.org , when the built-in java KafakContainer is used, the value is already set inside file `/testcontainers_start.sh`:
```
export KAFKA_ADVERTISED_LISTENERS='PLAINTEXT://localhost:49575,BROKER://192.168.112.3:9092'
```


# Links

* Consumer Configurations https://docs.confluent.io/platform/current/installation/configuration/consumer-configs.html
* Java Kafka Client https://kafka.apache.org/28/javadoc/org/apache/kafka/clients/consumer/package-summary.html
* Purging Kafka Topic https://www.baeldung.com/kafka-purge-topic
* Python kafka.TopicPartition() Examples https://www.programcreek.com/python/example/98439/kafka.TopicPartition
* Transactions in Apache Kafka https://www.confluent.io/blog/transactions-apache-kafka/
* Exactly-Once Semantics Are Possible: Here’s How Kafka Does It https://www.confluent.io/blog/exactly-once-semantics-are-possible-heres-how-apache-kafka-does-it/
