Apache Kafka

* [CLI](Kafka-cli.md)
* [Consumer groups](kafka-consumer-group.md)
* [Kafka client IllegalStateException: No current assignment for partition XYZ](kafka-client-seek-exception.md)
