#### Contents

* [Gui clients](#gui_clients)
* [psql](#psql)
* Docker image
   * [Where to Store Data](#where_to_store_data)
   * [Restore database](#restore_database)
* [PostgreSQL documentation and tutorials](postgresql_documentation_and_tutorials)
* Tips & tricks
   * [Import CSV file data](#import_csv_file_data)
   * [Display procedure source code](#display_procedure_source_code)
* [PostgreSQL portable on Windows 7](#postgresql_portable_on_windows_7)

### Gui clients

```
sudo apt-get install pgadmin3
```

# psql

##### connect to database
If postres is running in docker: `docker exec -it container sh`
```
psql -h localhost -U user database
```

##### psql commands
* \q quit
* \l list databases
* \dt list tables
* \d table structure
* \d+ extended table structure

##### Schema
```
pg_dump -s -h localhost -U user database
```

##### list all foregin keys
```sql
SELECT
    tc.table_schema, 
    tc.constraint_name, 
    tc.table_name, 
    kcu.column_name, 
    ccu.table_schema AS foreign_table_schema,
    ccu.table_name AS foreign_table_name,
    ccu.column_name AS foreign_column_name 
FROM 
    information_schema.table_constraints AS tc 
    JOIN information_schema.key_column_usage AS kcu
      ON tc.constraint_name = kcu.constraint_name
      AND tc.table_schema = kcu.table_schema
    JOIN information_schema.constraint_column_usage AS ccu
      ON ccu.constraint_name = tc.constraint_name
      AND ccu.table_schema = tc.table_schema
WHERE tc.constraint_type = 'FOREIGN KEY'  AND tc.table_name = 'mytable';
```

# Docker image

### Where to Store Data

* Let Docker manage the storage of your database data using its own internal volume management.
* Create a data directory on the host system (outside the container) and mount as [volume](https://docs.docker.com/storage/volumes/)
   ```
   mkdir /my/own/datadir
   docker run --name some-postgres -v /my/own/datadir:/var/lib/postgresql/data -d postgres:tag
   ```

### Restore database

* Prerequisite: the backup file is /my/path/backupfile.sql.gz
* Modify docker run from previous paragraph:
   ```
   docker run --name some-postgres -v /my/own/datadir:/var/lib/postgresql/data -v /my/path/backupfile.sql.gz:/tmp/backupfile.sql.gz -d postgres:tag
   ```
* Restore database
   ```
   docker exec -it some-postgres /bin/sh
   gunzip -c /tmp/backupfile.sql.gz |  psql -U postgres postgres
   ```

# PostgreSQL documentation and tutorials
* PostgreSQL Tutorials:
   * http://www.postgresqltutorial.com/
   * https://www.tutorialspoint.com/postgresql/index.htm
   * https://www.techonthenet.com/postgresql/index.php
* PostgreSQL Documentation
   * https://www.postgresql.org/docs/current/index.html

# Tips & tricks

### Import CSV file data
Create your table:
```
CREATE TABLE currency (
    country varchar(50),
    currency varchar(50),
    iso_code char(3)      -- ISO-4127
);
```

Copy data from your CSV file to the table. Needs permission to use COPY (which work on the db server):
```
COPY currency FROM '/path/to/csv/currency_data.txt' WITH (FORMAT csv);
```

in _psql_ client you can use \copy instead (which works in the db client only)
```
\copy currency FROM '/path/to/csv/currency_data.txt' DELIMITER ',' CSV
```

You can also specify the columns to read:
```
\copy currency (country,cyrrency,iso_code) FROM '/path/to/csv/currency_data.txt' DELIMITER ',' CSV
```

### Display procedure source code
```
SELECT prosrc FROM pg_proc WHERE proname='your procedure name here';
```
The precedure names are always lowercase in the `pg_proc` table.


# PostgreSQL portable on Windows 7

1. Get binaries for Windows. Choose the zip archive.<br>
   https://www.enterprisedb.com/download-postgresql-binaries

2. Copy the batch files into the root of the postgresql folder.

3. Adjust PDGATA variables and create the folder. It is better to have the data on local disk drive.

4. For first time use, uncomment out the (initdb step)

5. Run the batch file `start-server.bat` to start database.

6. Run the batch file `psql-cmd.bat` to have command prompt for db management.

7. There is pgAdmin4.exe (or pgAdmin3.exe for older versions) in the bin subdirectory.