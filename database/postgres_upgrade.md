# Upgrade postgres between major versions

* **pg_upgrade** https://www.postgresql.org/docs/current/pgupgrade.html
* Upgrading PostgreSQL 9.6 to PostgreSQL 13 https://www.migops.com/blog/upgrading-postgresql-9-6-to-postgresql-13/
