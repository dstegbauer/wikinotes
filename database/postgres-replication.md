# Postgres Replication

Streaming Replication (SR) provides the capability to continuously ship and apply the WAL XLOG records to some number of standby servers in order to keep them current.

## Links

* PostgreSQL Streaming Replication Documentation http://www.postgresql.org/docs/current/static/warm-standby.html
* Binary Replication Tutorial provides an introduction to using this replication feature. https://wiki.postgresql.org/wiki/Binary_Replication_Tutorial
* The related but independent Hot Standby feature. https://wiki.postgresql.org/wiki/Hot_Standby
* 2 Easy Methods for PostgreSQL Streaming Replication https://hevodata.com/learn/postgresql-streaming-replication/

* PostgreSQL Replication and Automatic Failover Tutorial https://www.enterprisedb.com/postgres-tutorials/postgresql-replication-and-automatic-failover-tutorial
* Replication in PostgreSQL Example https://riptutorial.com/postgresql/example/19480/replication-in-postgresql
* How to Setup PostgreSQL 11 Replication https://linuxhint.com/setup_postgresql_replication/
* Introduction to Replication in PostgreSQL https://dbsguru.com/introduction-to-replication-in-postgresql/

