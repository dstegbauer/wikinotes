### Table of contents

* Documentation
   * [Documentation Links](#documentation-links)
   * [Articles](#articles)
   * [Books](#books)
* [Configuration](#configuration)
* Tricks
   * [SSH keypair authentication](#ssh-keypair-authentication)
   * [Stored password on windows](#stored-password-on-windows)
   * [Fetch from "common" repo, push to "private" repo](#fetch-from-common-repo-push-to-private-repo)
   * [Remove last commit from remote git repository](#remove-last-commit-from-remote-git-repository)
   * [List files modified in branch](#list-files-modified-in-branch)
   * [Diff single commit](#diff-single-commit)
   * [Git log to list commits only for a specific branch](#git-log-to-list-commits-only-for-a-specific-branch)
   * [Copy file from one branch to another](#copy-file-from-one-branch-to-another)
   * [Amend older Git commit](#amend-older-git-commit)

# Documentation

### Documentation Links
* https://git-scm.com/docs
* https://help.github.com/
* https://githowto.com/

### Articles

* Connecting to GitHub with SSH https://help.github.com/en/articles/connecting-to-github-with-ssh
   * [Checking for existing SSH keys](https://help.github.com/en/articles/checking-for-existing-ssh-keys)
   * [Generating a new SSH key and adding it to the ssh-agent](https://help.github.com/en/articles/generating-a-new-ssh-key-and-adding-it-to-the-ssh-agent)
   * [Working with SSH key passphrases](https://help.github.com/en/articles/working-with-ssh-key-passphrases)
* Git Stash Tutorial  https://www.atlassian.com/git/tutorials/saving-changes/git-stash
 

###  Books

The **Pro Git** book, by Scott Chacon and Ben Straub. Released under the Creative Commons Attribution Non Commercial Share Alike 3.0 license.
Online + pdf + epub + mobi:<br>
https://git-scm.com/book/en/v2

The **Git Magic** book by Ben Lynn. Released under the GNU General Public License version 3
Online + pdf + epub:<br>
http://www-cs-students.stanford.edu/~blynn/gitmagic/index.html

Free PDF + EPUB reader for windows (portable app):<br>
https://portableapps.com/apps/office/sumatra_pdf_portable

# Configuration

#### set Meld as git mergetool and git difftool (linux)
```
git config --global merge.tool meld
git config --global diff.tool meld
```

additionally on Windows
```
git config --global mergetool.meld.path "C:/Programs/MeldPortable/App/Meld/Meld.exe"
git config --global difftool.meld.path "C:/Programs/MeldPortable/App/Meld/Meld.exe"
```

See also
* https://git-scm.com/docs/git-mergetool
* https://git-scm.com/docs/git-difftool
* Portable Apps https://portableapps.com/apps
* Meld Portable https://sourceforge.net/projects/mwayne/files/MeldPortable/


# Tricks

### SSH keypair authentication

On Linux Mint Mate 19:
* Generate key pair `ssh-keygen -t rsa -b 4096 -C "your_email@example.com"` (substituting in your GitHub email address)
* Add your SSH __private__ key to the ssh-agent `ssh-add ~/.ssh/id_rsa`
* Add the __public__ key to GitHub: Settings → SSH and GPG keys → New SSH key. Paste the key content here

See also: Connecting to GitHub with SSH https://help.github.com/en/articles/connecting-to-github-with-ssh


### Stored password on windows

Open Control panel (the classic one, not Windows 10 settings). Go to User Acounts → Credential Manager → Windows Credentials. Find the guilty one, remove it.

### Fetch from "common" repo, push to "private" repo

1. clone the "fetching" repo
2. `git remote -v` print all remotes
3. `git remote set-url --push origin another_repo` change the remote repo just for the pushes
4. `git remote -v` print all remotes


### Remove last commit from remote git repository

Be careful that this will create an "alternate reality" for people who have already fetch/pulled/cloned from the remote repository.
1. remove commit locally:  `git reset HEAD^`
2. force-push the new HEAD commit: `git push origin +HEAD`

If you want to still have it in your local repository and only remove it from the remote, then you can use:<br/>
`git push origin +HEAD^:<name of your branch, most likely 'master'>`


### List files modified in branch

```
git diff --name-only some-other-branch
```
will show you what files are different between your current branch and _some-other-branch_ (it really could be any commit at all, or anything that resolves to one (tags, etc.))

### Diff single commit

```
git show 15dc8
```
shows the log message for the commit, and the diff of that particular commit.
If you want only diff, you can use `git diff-tree -p 15dc8`

```
git diff 15dc8^!
```
_The r1^@ notation means all parents of r1. r1^! includes commit r1 but excludes all of its parents._ This means that you can use 15dc8^! as a shorthand for 15dc8^..15dc8 anywhere in git where revisions are needed. For diff command the git diff 15dc8^..15dc8 is understood as git diff 15dc8^ 15dc8, which means the difference between parent of commit (15dc8^) and commit (15dc8).


### Git log to list commits only for a specific branch

Exclude the commits reachable by master and gives me what I want, BUT I would like to avoid the need of knowing the other branches names.
```
git log mybranch --not master
```

Ignore merges from master
```
git log --oneline --no-merges master..
```

### Copy file from one branch to another

Run this from the branch where you want the file to end up:
```
git checkout otherbranch myfile.txt
```
General formulas:
```
git checkout <commit_hash> <relative_path_to_file_or_dir>
git checkout <remote_name>/<branch_name> <file_or_dir>
```
Some notes (from comments):
* Using the commit hash you can pull files from any commit
* This works for files and directories
* overwrites the file myfile.txt and mydir
* Wildcards don't work, but relative paths do
* Multiple paths can be specified

### Amend older Git commit

* https://stackoverflow.com/questions/8824971/how-to-amend-older-git-commit
* http://git-scm.com/book/en/Git-Tools-Rewriting-History

Rebase interactive "one before" to specific commit.
The _`HEAD^^^`_ means "3 commits before head".
Instead of _`HEAD^^^`_ you could use the hash of a commit, i.e. `d2b18867^` (note the extra `^`).
Some shells (like zsh) parse `^` as a pattern, you can use `~` instead in that case.
```
git rebase -i HEAD^^^
```

Now mark the ones you want to amend with `edit` or `e` (replace _pick_). Now save and exit.

Now make your changes, then
```
git add .
git rebase --continue
```
Note - you don't have to git commit after git add -A, simply git rebase --continue will keep your changes.

Push your changes upstream
```
git push -f
```
