# Git hooks

Links
* https://git-scm.com/book/en/v2/Customizing-Git-Git-Hooks
* https://githooks.com/
* https://verdantfox.com/blog/view/how-to-use-git-pre-commit-hooks-the-hard-way-and-the-easy-way

Git hooks are scripts that Git executes before or after events such as: commit, push, and receive.
Git hooks are a built-in feature - no need to download anything. Git hooks are run locally.

Every Git repository has a `.git/hooks` folder with a sample script for each hook you can bind to.

##### Important client side hooks

Each hook is executed with CWD set to repo root.

| Hook file name | Desc                                                     |
|----------------|----------------------------------------------------------|
| pre-commit     | is run first, before you even type in a commit message   |
| commit-msg     | takes one parameter: path to a  with the commit message  |


## Ktlint Gradle Kotlin hook

Script [pre-commit_ktlint-gradle-kotlin.sh](hooks/pre-commit_ktlint-gradle-kotlin.sh)
checks only modified files, so the hook can be introduced in the middle od development.

## Checkstyle Maven Java hook

Script [pre-commit_checkstyle-maven-java.sh](hooks/pre-commit_checkstyle-maven-java.sh)
checks only modified files, so the hook can be introduced in the middle od development.
In addition, it uses custom checkstyle config in `src/main/resources/checkstyle.xml`

##### Notes
The `includes` is a comma delimited list of patterns. No paths on filesystem!!

Commandline arguments are counter-intuitive, they are sometimes named completely differently.
See https://maven.apache.org/plugins/maven-checkstyle-plugin/checkstyle-mojo.html and look for _User property is_

| XML element                  | command line                      |
|------------------------------|-----------------------------------|
| <configLocation>             | checkstyle.config.location        |
| <outputFile>                 | checkstyle.output.file            |
| <outputFileFormat>           | checkstyle.output.format          |
| <propertiesLocation>         | checkstyle.properties.location    |
| <rulesFiles>                 | checkstyle.output.rules.file      |
| <suppressionsFileExpression> | checkstyle.suppression.expression |

