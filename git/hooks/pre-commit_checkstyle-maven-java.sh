#!/bin/sh

######## CHECKSTYLE MAVEN HOOK START ########
CHANGED_FILES=$(git --no-pager diff --name-status --no-color --cached | awk '$1 != "D" && $NF ~ /\.java?$/ { print $NF }')

if [ -z "$CHANGED_FILES" ]; then
    echo "No Java staged files."
    exit 0
fi;

echo "Running checkstyle"
INCLUDES=''
for JFILE in $CHANGED_FILES; do
  INCLUDES="**\\/$(basename $JFILE),$INCLUDES"
done

mvn checkstyle:check \
  -Dcheckstyle.includeResources=false \
  -Dcheckstyle.includeTestResources=false \
  -Dcheckstyle.config.location=src/main/resources/checkstyle.xml \
  -Dcheckstyle.includes="${INCLUDES%,}"
checkstyle_result=$?
[ $checkstyle_result -ne 0 ] && exit $checkstyle_result
######## CHECKSTYLE MAVEN HOOK END ########

exit 0
