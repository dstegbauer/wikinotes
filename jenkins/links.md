# Official documentation

For a Pipeline, Groovy code **always** executes on controller.

* Pipeline syntax https://www.jenkins.io/doc/book/pipeline/syntax/
* Pipeline examples https://www.jenkins.io/doc/pipeline/examples/ _(some look outdated)_
* Pipeline Steps Reference https://www.jenkins.io/doc/pipeline/steps/ _particularly:_
  * Artifactory Plugin
  * Docker Pipeline
  * Docker plugin
  * Git plugin
  * Git Push Plugin
  * GitHub Pull Request Builder
  * Jenkins Core https://www.jenkins.io/doc/pipeline/steps/core/
  * Pipeline Utility Steps https://www.jenkins.io/doc/pipeline/steps/pipeline-utility-steps/
  * Pipeline: Basic Steps https://www.jenkins.io/doc/pipeline/steps/workflow-basic-steps/
  * Pipeline: Build Step https://www.jenkins.io/doc/pipeline/steps/pipeline-build-step/
  * Pipeline: Declarative https://www.jenkins.io/doc/pipeline/steps/pipeline-model-definition/
  * Pipeline: Groovy https://www.jenkins.io/doc/pipeline/steps/workflow-cps/
  * Pipeline: Groovy Libraries https://www.jenkins.io/doc/pipeline/steps/pipeline-groovy-lib/
  * Pipeline: Input Step https://www.jenkins.io/doc/pipeline/steps/pipeline-input-step/
  * Pipeline: Multibranch https://www.jenkins.io/doc/pipeline/steps/workflow-multibranch/
  * Pipeline: Nodes and Processes https://www.jenkins.io/doc/pipeline/steps/workflow-durable-task-step/
  * Pipeline: SCM Step https://www.jenkins.io/doc/pipeline/steps/workflow-scm-step/
  * Pipeline: Stage Step https://www.jenkins.io/doc/pipeline/steps/pipeline-stage-step/
  * Pyenv Pipeline Plugin https://www.jenkins.io/doc/pipeline/steps/pyenv-pipeline/
  * SSH Agent Plugin https://www.jenkins.io/doc/pipeline/steps/ssh-agent/
  * SSH Pipeline Steps https://www.jenkins.io/doc/pipeline/steps/ssh-steps/

* Jenkins Blog: Welcome to the Matrix https://www.jenkins.io/blog/2019/11/22/welcome-to-the-matrix/
