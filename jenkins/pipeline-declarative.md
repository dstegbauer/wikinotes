# Declarative pipeline bits and pieces

### Signal success or failure

> ℹ️ despite the comments in the code below,
> the post steps are executed indeed even if you fail with `error()`

```groovy
stage('stage name') {
    steps {
        script {
            def status = someFunc() 

            if (status != 0) {
                // Use SUCCESS FAILURE or ABORTED
                currentBuild.result = "FAILURE"
                throw new Exception("Throw to stop pipeline")
                // do not use the following, as it does not trigger post steps (i.e. the failure step)
                // error "your reason here"
            }
        }
    }
    post {
        success {
            script {
                echo "success"
            }
        }
        failure {
            script {
                echo "failure"
            }
        }
    }            
}
```