# Groovy in jenkins pipelines

##### Links
 * [Using Jenkins with Groovy scripts: A Practical Guide (with examples)](https://www.slingacademy.com/article/using-jenkins-with-groovy-scripts-a-practical-guide-with-examples/)

### Check if a file exists

```groovy
if (fileExists('file')) {
    echo 'Yes'
} else {
    echo 'No'
}
```

When stored in variable, then braces can be ommited
```groovy
def exists = fileExists 'file'
```

### Fail stage programmatically

```groovy
error "Error message"
```
