# load: Evaluate a Groovy source file into the Pipeline script

Takes a filename in the workspace and runs it as Groovy source text.

# What works for me

## just a commands

file `src/jenkins/just_commands.groovy`:
```groovy
echo 'loaded from a file'
echo 'another line'
```

pipeline snippet
```groovy
stage('load test') {
    steps {
        load 'src/jenkins/just_commands.groovy'
    }
}
```

## entire (declarative) pipeline

Cons: it uses two different worker nodes

file `src/jenkins/pipeline.groovy`:
```groovy
pipeline {
    agent {
        label 'ec2-docker-small-spot'
    }

    stages {
        stage('info') {
            steps {
                echo 'I am complete pipeline loaded from file'
                echo "env.GIT_BRANCH = ${env.GIT_BRANCH}"
                sh 'printenv'
                sh 'ls'
                script {
                    def pom = readMavenPom file: 'pom.xml'
                    echo pom.version
                    def branch = scm.branches[0].name
                    echo "branch is ${branch}"
                }
            }
        }
    }
}
```

file `Jenkinsfile-load`
```groovy
#!groovy
node('ec2-docker-small-spot') {
    // without checkout there is nothing to load
    checkout scm
    // the load command must be inside the node
    load 'src/jenkins/pipeline.groovy'
}
```


# What does NOT work

## stages

file `stages.groovy`:
```groovy
stages {
    stage('a') {
        echo 'I am complete pipeline loaded from file'
    }
}
```

file `Jenkinsfile-load`
```groovy
#!groovy
pipeline {
    agent {
        label 'ec2-docker-medium-spot'
    }
    load 'src/jenkins/stages.groovy'
}
```

error:
```
WorkflowScript: 2: Missing required section "stages" @ line 2, column 1.
   pipeline {
   ^

```


## stage

file `src/jenkins/whole_stage.groovy`:
```groovy
stage('load whole stage') {
    steps {
        echo 'I am complete stage loaded from file'
    }
}
```

pipeline snippet
```groovy
stage('a') { ... }
load 'src/jenkins/whole_stage.groovy'
stage('b') { ... }
```


# From official documentation
The loaded file can contain statements at top level or just load and run a closure. For example:

    def pipeline
    node('agent') {
        pipeline = load 'pipeline.groovy'
        pipeline.functionA()
    }
    pipeline.functionB()
    
Where pipeline.groovy defines functionA and functionB functions (among others) before ending with return this;

path : String
Current directory (pwd()) relative path to the Groovy file to load.

