# Jenkinsfile built-in environment variables

* https://phoenixnap.com/kb/jenkins-environment-variables

### env
The `env` map is _jenkins_ environment.
The items are accessible to shell command as shell environment variables.
It is shared between stages, you can set item in a stage and use it in following stages.

##### List all
In web browser navigate to the address: `[Jenkins URL]/env-vars.html`

##### Read
```groovy
echo "The current build number is ${env.BUILD_NUMBER}"
```

##### Set
Declarative
```groovy
environment {
    VARIABLE_NAME = "variable value"
}
```

Scripted
```groovy
env.VARIABLE_NAME = "variable value"
```

In a block only
```groovy
withEnv(["TEST_VARIABLE=TEST_VALUE"]) {
    echo "The value of TEST_VARIABLE is ${env.TEST_VARIABLE}"
}
```

##### env.BRANCH_NAME
Current git branch name.
* Pipeline ⇒ env.BRANCH_NAME return branch `null`
* Multibranch Pipeline ⇒ env.BRANCH_NAME return branch `name`

Workaround for "normal" pipeline:
```groovy
String getCurrentBranch () {
    return sh (
        script: 'git rev-parse --abbrev-ref HEAD',
        returnStdout: true
    ).trim()
}
```

##### env.GIT_BRANCH
The remote branch name, if any.
* Pipeline ⇒ resolves to `origin/{BRANCH}`
* Multibranch Pipeline ⇒ resolves to `{BRANCH}` (no `origin/`).


### env.WORKSPACE
The absolute path of the directory assigned to the build as a workspace.
`WORKSPACE` only works if it is used in an agent (node). Otherwise, it comes out as `null`.